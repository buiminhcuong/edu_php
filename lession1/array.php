<html>
	<head>
		<style>
			#div1 {
				background-color: blue;
				color: white;
				font-size: 40px;
			}

			body {
				margin: 0px;
			}
		</style>
	</head>
	<body>
		<div id="div1">
			Lession1: Array
		</div>
		<h2>Introduction</h2>
		<p>
			<?php
				$a[0] = "Hello";
				$a[1] = "world";

				echo($a[0] . " " . $a[1]);
			?>
		</p>

		<h2>Index</h2>
		<p>
			<?php
				$a[0] = "value1";
				$a["key2"] = "value2";
				$a[5] = "value3";
				$a[2] = "value4";
				//auto index = ?
				$a[] = "value5";

				echo("printout \"value5\": " . $a[6]);
			?>
		</p>

		<h2>Multi-dimesion array</h2>
		<p>
			<?php
				$a2[0]["hoten"] = "Nguyen Van A";
				$a2[0]["diachi"] = "Ha Noi";

				$a2[1]["hoten"] = "Nguyen Van B";
				$a2[1]["diachi"] = "hai Phong";

				$a2[2]["hoten"] = "Nguyen Van C";
				$a2[2]["diachi"] = "Can Tho";

				$a2[3]["hoten"] = "Nguyen Van D";
				$a2[3]["diachi"] = "Da Nang";
			?>
		</p>

		<h2>Array processing: for with index</h2>
		<table>
			<tr>
				<td>Ho Ten</td>
				<td>Dia Chi</td>
			</tr>
			<?php
				for($i = 0; $i < sizeof($a2); $i++) {
			?>
				<tr>
					<td><?= $a2[$i]["hoten"] ?></td>
					<td><?= $a2[$i]["diachi"] ?></td>
				</tr>
			<?php
				}
			?>
		</table>

		<h2>Array processing: foreach</h2>
		<table>
			<tr>
				<td>Ho Ten</td>
				<td>Dia Chi</td>
			</tr>
			<?php
				foreach($a2 as $sv) {
			?>
				<tr>
					<td><?= $sv["hoten"] ?></td>
					<td><?= $sv["diachi"] ?></td>
				</tr>
			<?php
				}
			?>
		</table>

		<h2>Array: Initialize using array() function</h2>
		<p>
			<?php
				$a3 = array(
						array("hoten" => "Nguyen Van A","diachi" => "Ha Noi"),
						array("hoten" => "Nguyen Van B","diachi" => "Hai Phong"),
						array("hoten" => "Nguyen Van C","diachi" => "Can Tho"),
						array("hoten" => "Nguyen Van D","diachi" => "Da Nang")
					);

				echo ($a3[1]["hoten"]);
			?>
		</p>
	</body>
</html>