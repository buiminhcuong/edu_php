<html>
	<head>
		<style>
			#div1 {
				background-color: blue;
				color: white;
				font-size: 40px;
			}

			body {
				margin: 0px;
			}
		</style>
		<script type="text/javascript" src="../common/jquery-1.11.3.min.js"> </script>
	</head>
	<body>
		<div id="div1">
			Lession1: Hello world!!!
		</div>
		<h2>constants</h2>
		<p>
			<?php
				define("CLASS_NAME", "CNPM K53");
				echo("class: " . CLASS_NAME);
			?>
		</p>

		<h2>variables</h2>
		<p>
			<?php
				//variables
				$name = "Nguyen Van An";
				$class = "CNPM";
				$university = "GTVT";
				echo("Name: $name; Class: $class; University: $university");
			?>
		</p>

		<h2>dynamic variables</h2>
		<p>
			<?php
				//dynamic variables
				$a = "hello";
				$$a = "world";
				echo("$a {$$a}");
			?>
		</p>

		<h2>string</h2>
		<p>
			<?php
				//string
				$a = "hello";
				echo("$a world");
				echo("<br/>");
				echo('$a world');
			?>
		</p>

		<h2>special characters</h2>
		<p>
			<?php
				echo("He says \"I'm awesome!\"");
				echo("<br/>");				
				echo("To espace special characters in PHP use \\");
			?>
		</p>

		<h2>string concat</h2>
		<p>
			<?php
				echo("Use ." . " to concat " . "string");
			?>
		</p>
	</body>
</html>