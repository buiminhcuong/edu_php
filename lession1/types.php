<html>
	<head>
		<style>
			#div1 {
				background-color: blue;
				color: white;
				font-size: 40px;
			}

			body {
				margin: 0px;
			}
		</style>
	</head>
	<body>
		<div id="div1">
			Lession1: Types
		</div>

		<h2>Type cast</h2>
		<p>
			<?php
				$x = "123abc";
				echo("type of \$x = " . gettype($x));
				echo("<br/>");

				$x = (int) $x;

				echo("value of x: " .$x);
				echo("<br/>");

				echo("type of \$x = " . gettype($x));
				echo("<br/>");

				$y = "abc";
				$y = (bool) $y;
				if($y) {
					echo("\$y = " . $y);	
				}
			?>
		</p>

		<h2>unset</h2>
		<p>
			<?php
				$x = "123abc";
				unset($x);

				if($x == null) {
					echo("\$x = <i>null</i>");
				}
			?>
		</p>
</html>