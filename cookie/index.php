<?php
	if(isset($_POST["submit"]) && $_POST["submit"] == "OK") {
		$ckName  = $_POST["cookie_name"];
		$ckValue  = $_POST["cookie_value"];
		if($ckName != "") {
			setcookie($ckName, $ckValue, time() + 3600);
			header("location:index.php");
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="common/style.css">
</head>
<body>
	<div id="div1">
		Cookie
	</div>
	<h1>
		Working with cookie
	</h1>
	<p>
		<div>
			<form method="POST" action="">
				<table>
					<tr>
						<td>Name:</td>
						<td><input type="text" name="cookie_name" /></td>
					</tr>
					<tr>
						<td>Value:</td>
						<td><input type="text" name="cookie_value" /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" name="submit" value="OK" /></td>
					</tr>
				</table>
			</form>
		</div>
		<table class="tbl-cookie">
			<thead>
				<th>Name</th>
				<th>Value</th>
			</thead>
			<?php
				foreach ($_COOKIE as $k => $v) {
			?>
				<tr>
					<td><?= $k ?></td>
					<td><?= $v ?></td>
				</tr>
			<?php
				}
			?>
			</tbody>
		</table>
	</p>
</body>
</html>