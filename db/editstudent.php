<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.form-validator.min.js"></script>
	<style>
		.student-data input.error{
			border-color: red;
		}
	</style>
</head>
<body>
<?php
		$id = $_GET["id"];

		$name = "";
		$description = "";
		$address = "";
		$mathmark = "";
		$physicalmark = "";
		$chemistrymark = "";

		$dbHost = "localhost";
		$dbUser = "root";
		$dbPass = "";
		$dbName = "edu";

		$error = "";

		//B1: ket noi CSDL
		mysql_connect($dbHost,$dbUser,$dbPass)
			or die("Cannot connect to database");

		//B2: chon CSDL
		mysql_select_db($dbName)
			or die("Cannot select db $dbName");

		mysql_set_charset("utf8");

		if(isset($_POST["submit"])) {
			$name = $_POST["name"];
			$description = $_POST["description"];
			$address = $_POST["address"];
			$mathmark = $_POST["mathmark"];
			$physicalmark = $_POST["physicalmark"];
			$chemistrymark = $_POST["chemistrymark"];

			if($mathmark > 10 || $mathmark < 0) {
				$error = "Math mark must be between 0 and 10;";
			}

			if($physicalmark > 10 || $physicalmark < 0) {
				$error = $error . "Physical must be between 0 and 10";
			}

			if($chemistrymark > 10 || $chemistrymark < 0) {
				$error = $error . "Chemistry must be between 0 and 10";
			}

			if($error == ""){
				$sql = "UPDATE `student` SET `name`='$name',`address`='$address',`description`='$description'," . 
				"`mathmark`=$mathmark,`physicalmark`=$physicalmark,`chemistrymark`=$chemistrymark WHERE id = $id";

				$result = mysql_query($sql);

				if($result == true) {
					if(mysql_affected_rows() == 0) {
						$error = "Cannot update student with id = $id";
					}
					else {
						header('location:studentdemo.php');
					}
				}
				else {
					$error ="Cannot add new student: " . mysql_error();	
				}

				
			}
		}
		else {
			$sql = "SELECT * FROM student WHERE id = $id"; 
			$result = mysql_query($sql);
			if(mysql_num_rows($result) == 0) {
				$error = "Student with id $id not found";
			}
			else {
				$row = mysql_fetch_assoc($result);
				$name = $row["name"];
				$description = $row["description"];
				$address = $row["address"];
				$mathmark = $row["mathmark"];
				$physicalmark = $row["physicalmark"];
				$chemistrymark = $row["chemistrymark"];
			}
		}

	mysql_close();
?>
	<a href="studentdemo.php">Back to list</a>
	<div>
		<?= $error ?>
	</div>
	<form method="POST" id="form-sutdent">
		<table class="student-data">
			<tr>
				<td>
					Name:
				</td>
				<td>
					<input type="text" name="name" id="name" data-validation="length" data-validation-length="3-50" 
		 data-validation-error-msg="Name has to be an value (3-50 chars)" value="<?= $name ?>"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Description:
				</td>
				<td>
					<input type="text" name="description" id="description" value="<?= $description ?>"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Address:
				</td>
				<td>
					<input type="text" name="address" id="address" value="<?= $address ?>"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Math mark:
				</td>
				<td>
					<input type="text" name="mathmark" id="mathmark" data-validation="number" data-validation-allowing="float,range[0;10]" 
					data-validation-error-msg="Mark must be between 0-10" value="<?= $mathmark ?>"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Physical mark:
				</td>
				<td>
					<input type="text" name="physicalmark" id="physicalmark" data-validation="number" data-validation-allowing="float,range[0;10]" 
					data-validation-error-msg="Mark must be between 0-10" value="<?= $physicalmark ?>"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Chemistry mark:
				</td>
				<td>
					<input type="text" name="chemistrymark" id="chemistrymark" data-validation="number" data-validation-allowing="float,range[0;10]" 
					data-validation-error-msg="Mark must be between 0-10" value="<?= $chemistrymark ?>"> </input>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<input type="submit" name="submit" value="Edit"> </input>
				</td>
			</tr>
		</table>
	</form>
	<script>
		$.validate();
	</script>
</body>
</html>