<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.form-validator.min.js"></script>
	<style>
		.student-data input.error{
			border-color: red;
		}
	</style>
</head>
<body>
<?php
	if(isset($_POST["submit"])) {
		$name = mysql_real_escape_string($_POST["name"]);
		$description = $_POST["description"];
		$address = $_POST["address"];
		$mathmark = $_POST["mathmark"];
		$physicalmark = $_POST["physicalmark"];
		$chemistrymark = $_POST["chemistrymark"];

		$dbHost = "localhost";
		$dbUser = "root";
		$dbPass = "";
		$dbName = "edu";

		$error = "";

		if($mathmark > 10 || $mathmark < 0) {
			$error = "Math mark must be between 0 and 10;";
		}

		if($physicalmark > 10 || $physicalmark < 0) {
			$error = $error . "Physical must be between 0 and 10";
		}

		if($chemistrymark > 10 || $chemistrymark < 0) {
			$error = $error . "Chemistry must be between 0 and 10";
		}

		if($error == ""){
			//B1: ket noi CSDL
			mysql_connect($dbHost,$dbUser,$dbPass)
				or die("Cannot connect to database");

			//B2: chon CSDL
			mysql_select_db($dbName)
				or die("Cannot select db $dbName");

			mysql_set_charset("utf8");

			$sql = "INSERT INTO `student`(`name`, `address`, `description`, `mathmark`, `physicalmark`, `chemistrymark`) VALUES ('$name','$address','$description',$mathmark,$physicalmark,$chemistrymark)";

			$result = mysql_query($sql);

			if($result == true) {
				header('location:studentdemo.php');
			}
			else {
				echo ("Cannot add new student: " . mysql_error());	
			}

			mysql_close();
		}
		else {
			echo($error."<br/>");
		}
	}
?>
	<a href="studentdemo.php">Back to list</a>
	<form method="POST" id="form-sutdent">
		<table class="student-data">
			<tr>
				<td>
					Name:
				</td>
				<td>
					<input type="text" name="name" id="name" data-validation="length" data-validation-length="3-50" 
		 data-validation-error-msg="Name has to be an value (3-50 chars)"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Description:
				</td>
				<td>
					<input type="text" name="description" id="description"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Address:
				</td>
				<td>
					<input type="text" name="address" id="address"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Math mark:
				</td>
				<td>
					<input type="text" name="mathmark" id="mathmark" data-validation="number" data-validation-allowing="range[0;10]" data-validation-error-msg="Mark must be between 0-10"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Physical mark:
				</td>
				<td>
					<input type="text" name="physicalmark" id="physicalmark" data-validation="number" data-validation-allowing="range[0;10]" data-validation-error-msg="Mark must be between 0-10"> </input>
				</td>
			</tr>
			<tr>
				<td>
					Chemistry mark:
				</td>
				<td>
					<input type="text" name="chemistrymark" id="chemistrymark" data-validation="number" data-validation-allowing="range[0;10]" data-validation-error-msg="Mark must be between 0-10"> </input>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<input type="submit" name="submit" value="Validate"> </input>
				</td>
			</tr>
		</table>
	</form>
	<script>
		$.validate();
	</script>
</body>
</html>