<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.form-validator.min.js"></script>
</head>
<body>
<?php
	$dbHost = "localhost";
	$dbUser = "root";
	$dbPass = "";
	$dbName = "edu";

	//B1: ket noi CSDL
	mysql_connect($dbHost,$dbUser,$dbPass)
		or die("Cannot connect to database");

	//B2: chon CSDL
	mysql_select_db($dbName)
		or die("Cannot select db $dbName");

	mysql_set_charset("utf8");

	//B3.1: truy van
	$result = mysql_query("select * from student");
	//B3.2: xy ly nguon tai nguyen tra ve

	//dem so ban ghi tra ve
	echo("number of selected record: " . mysql_num_rows($result). "<br/>");
?>
	<a href="addstudent.php">Add new student</a>
	<table>
		<tr>
			<td>Name</td>
			<td>Address</td>
			<td>Description</td>
			<td>Math mark</td>
			<td>Physical mark</td>
			<td>Chemistry mark</td>
		</tr>
<?php
	//hien thi danh sach ban ghi duoi dang bang
	while($row = mysql_fetch_array($result)) {
?>
		<tr>
			<td><?= $row["name"] ?></td>
			<td><?= $row["address"] ?></td>
			<td><?= $row["description"] ?></td>
			<td><?= $row["mathmark"] ?></td>
			<td><?= $row["physicalmark"] ?></td>
			<td><?= $row["chemistrymark"] ?></td>
			<td><a href="editstudent.php?id=<?= $row["id"] ?>"> Sửa </a></td>
			<td><a class="delete-link" data="<?= $row["id"] ?>" href="#"> Xóa </a></td>
		</tr>
<?php
	}

	//B4: dong ket noi CSDL
	mysql_close();
?>
	</table>
	<form id="delete-student" action="deletestudent.php" method="POST">
		<input type="hidden" id="delete-student-id" name="id">
	</form>
	<script>
		$('.delete-link').on('click', function(e) {
			if(confirm('Are you sure you want to delete selected student')) {
				console.log($(this).attr('data'));
				$("#delete-student-id").val($(this).attr('data'));
				$("#delete-student").submit();
			}

			e.preventDefault();
		});
	</script>
</body>
</html>