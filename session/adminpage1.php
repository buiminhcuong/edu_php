<?php
	session_start();
	if(!isset($_SESSION["username"])) {
		header("location:login.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="common/style.css">
</head>
<body>
	<div id="div1">
		My Admin page 1
	</div>
	<h3 style="text-align: right;">
		Hello <?= $_SESSION["username"] ?>, 
		<a href="logout.php">Logout</a>
	</h3>
	<h1>
		You must logged in to see this page
	</h1>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</p>
</body>
</html>