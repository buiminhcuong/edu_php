<?php
	session_start();
	if(isset($_SESSION["username"])) {
		header("location:adminpage1.php");
	}
	else {
		if(isset($_POST["submit"])) {
			$username = $_POST["username"];
			$password = $_POST["password"];
			//truy van csdl de kiem tra thong tin dang nhap
			if($username == "admin" && $password == "123") {
				$_SESSION["username"] = $username;
				header("location:adminpage1.php");
			}
			else {
				$error = "Invalid user or password";
			}
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" type="text/css" href="common/style.css">
	</head>
	<body>
		<div id="div1">
			Login
		</div>
		<h1>
			Enter your username and password
		</h1>
		<form method="POST">
			<table class="tbl-login">
			<?php
				if(isset($error)) {
			?>
				<tr class="tr-error">
					<td colspan="2"><?= $error ?></td>
				</tr>
			<?php
				}	
			?>
				<tr>
					<td>Username</td>
					<td><input type="text" name="username" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="password" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Login" /></td>
				</tr>
			</table>
		</form>
	</body>
</html>